<?php

if (empty($_POST['email'])) {
    $response['message'] = 'Email field is empty!';
    $response['status'] = 'error';
    echo json_encode($response);
    return;
}

$to = 'dev@dusted.com.au';
$subject = 'ECA "New message from '.$_POST['name'].'"';
$message = $_POST['message'];
$headers = 'Reply-To: '.$_POST['email'];

if(!mail($to, $subject, $message, $headers)){
    $response['status'] = 'error';
    echo json_encode($response);
    return;
};

$response['status'] = 'success';
echo json_encode($response);
return;

?>