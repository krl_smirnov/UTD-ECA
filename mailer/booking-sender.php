<?php

if (empty($_POST['email'])) {
    $response['message'] = 'Email field is empty!';
    $response['status'] = 'error';
    echo json_encode($response);
    return;
}


$to = 'dev@dusted.com.au';
$subject = 'ECA "Booking message ' . $_POST['name'] . '"';
$message = "<html><head></head><body><table border='2'>
    <tr>
        <td>First name</td>
        <td>" . $_POST['first_name'] . "</td>
    </tr>
    <tr>
        <td>Last name</td>
        <td>" . $_POST['last_name'] . "</td>
    </tr>
    <tr>
        <td>Email</td>
        <td>" . $_POST['email'] . "</td>
    </tr>
    <tr>
        <td>Email</td>
        <td>" . $_POST['phone'] . "</td>
    </tr>
    <tr>
        <td>IM INTERESTED IN</td>
        <td>" . $_POST['ship_type'] . "</td>
    </tr>
    <tr>
        <td>WITH</td>
        <td>" . $_POST['people_number'] . " people</td>
    </tr>
    <tr>
        <td>Visiting</td>
        <td>" . $_POST['tour'] . "</td>
    </tr>
    <tr>
        <td>AROUND THIS DATE</td>
        <td>" . $_POST['date'] . "</td>
    </tr>
    <tr>
        <td>PREFERRED FISHING STYLES?</td>
        <td>" . $_POST['fishing_style'] . "</td>
    </tr>
    <tr>
        <td>I WANT TO CATCH</td>
        <td>" . $_POST['desired_fish'] . "</td>
    </tr>
    <tr>
        <td>EXTRA ACTIVITIES?</td>
        <td>" . $_POST['extra_activities'] . "</td>
    </tr>
    <tr>
        <td>Message</td>
        <td>" . $_POST['message'] . "</td>
    </tr>
</table></body></html>";
$headers = 'Reply-To: ' . $_POST['email'].'\r\n';
$headers .= "MIME-Version: 1.0" . "\r\n";
$headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";

if (!mail($to, $subject, $message, $headers)) {
    $response['status'] = 'error';
    echo json_encode($response);
    return;
};

$response['status'] = 'success';
echo json_encode($response);
return;

?>