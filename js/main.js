(function ($) {
    $(document).on('ready', function () {
        toggleMenu();
        initFishSlider();
        mobileTourMenu();
        initShipSlider();
        initTourPagination();
        initToursTab();
        initShipTourTab('#tourShipTabs', 'active');
        initDatePicker();
        // initBookForm();
        initContactForm();
        hoverFleetTitle();
        // initScrollBar();
        initMultiSelect();
        addCheckBox();
        initPageScroll();
        initPopUp();
        initSelect2();
        initHomeAnimation();
        // initTourPagination();
        initHoverPagi();

        // if ($('body').hasClass('home')) {
        //     initMainPageAnimation();
        //     initMainPageVideo();
        // }


        if ($('body').hasClass('trips')) {
            initTourVideo();
        }
    });

    $(window).on('load', function () {
        // initPageScroll();
        initLoadAnimation();
        initPaginationSwitcher();
    });

    var closeMenu;

    function initHoverPagi() {
        var $pagiMenu = $('#pagiMenu');
        var pagiMenuLink = $pagiMenu.find('li a');

        pagiMenuLink.hover(
            function () {
                $(this).parent('li').addClass('hover');
            },
            function () {
                $(this).parent('li').removeClass('hover');
            }
        );
    }

    function initTourVideo() {
        var $videos = $('.video-wrapper').find('div');
        $.each($videos, function (i, video) {
            jwplayer($(video).attr('id')).setup({
                'file': $($videos[0]).attr('data-video'),
                'image': $($videos[0]).attr('data-poster'),
                width: '100%',
                height: '100%',
                controls: false,
                stretching: 'fill',
                autostart: true,
                repeat: true
            });
        });
    }

    function initPopUp() {
        $('.js-popup-button').magnificPopup({
            type: 'inline',
            midClick: true
        });
    }

    function initMultiSelect() {
        var $multipleSelect = $('select[multiple]');


        if ($multipleSelect.length === 0) {
            return;
        }
        $('select[multiple]').multiselect({
            name: 'Option Name 1',
            value: 'option-value-1',
            columns: 1,
            selectAll: true,
            minHeight: 80
        });

    }

    function addCheckBox() {
        var $msSelectAll = $('.ms-selectall');
        if ($msSelectAll.length === 0) {
            return;
        }
        var checkAll = $msSelectAll.before('<span class="check-all"></span>');
        var currentCheckAll = $msSelectAll.prev('.check-all');

        $msSelectAll.click(function () {
            $(this).prev('.check-all').toggleClass('selected');
        })

        currentCheckAll.click(function () {
            $(this).next().click();
        });

    }

    function initSelect2() {
        var $defaultSelect = $('select:not([multiple])');
        if ($defaultSelect.length === 0) {
            return;
        }
        $('select:not([multiple])').select2();
    }

    function initHomeAnimation() {
        if ($('body').hasClass('home')) {
            // initMainPageAnimation();
            initMainPageVideo();
        }
    }

    function initMainPageVideo() {
        var $playButton = $('#play-button'),
            $videos = $('.video-wrapper').find('div');

        jwplayer($($videos[0]).attr('id')).setup({
            'file': $($videos[0]).attr('data-video'),
            'image': $($videos[0]).attr('data-poster'),
            width: '100%',
            height: '100%',
            controls: false,
            stretching: 'fill',
            autostart: true,
            repeat: true
        });

        jwplayer($($videos[1]).attr('id')).setup({
            "file": $($videos[1]).attr('data-video'),
            "image": $($videos[1]).attr('data-poster'),
            width: '100%',
            height: '100%',
            controls: false,
            stretching: 'fill'
        });
        $playButton.click(function (e) {
            e.preventDefault();
            jwplayer($($videos[1]).attr('id')).play();
            $(this).addClass('start');
        });
        jwplayer($($videos[1]).attr('id')).on('play', function () {
            $playButton.addClass('start');
        });
        jwplayer($($videos[1]).attr('id')).on('pause', function () {
            $playButton.removeClass('start');
        });
        $('#video-overlay').click(function () {
            var state = jwplayer($($videos[1]).attr('id')).getState();
            switch (state) {
                case 'idle':
                case 'paused':
                    jwplayer($($videos[1]).attr('id')).play();
                    break;
                case 'playing':
                    jwplayer($($videos[1]).attr('id')).pause();
                    break;
            }
        });
    }


    function initLoadAnimation() {
        setTimeout(function () {
            $('body').addClass('loaded');
        }, 700)
    }

    function initScrollBar() {
        var scrollbar = $('.scrollbar-inner');
        if (scrollbar.length == 0) {
            return;
        }
        scrollbar.scrollbar();
    }

    function hoverFleetTitle() {
        var $rightLink = $('.fleet-heading-right'),
            // $rightBg = $('.fleet-right'),
            $leftLink = $('.fleet-heading-left'),
            $body = $('body');
        // $leftBg = $('.fleet-left');

        $rightLink.hover(
            function () {
                $body.addClass("active-fleet-right");
            },
            function () {
                $body.removeClass("active-fleet-right");
            }
        );

        $leftLink.hover(
            function () {
                $body.addClass("active-fleet-left");
            },
            function () {
                $body.removeClass("active-fleet-left");
            }
        );
    };

    function initPaginationSwitcher() {
        var $header = $('.tour-locations-header.animated-tour-pagination');
        var $tourPaginationList = $('.tour-list-menu');
        var $buttons = $header.find('.select-ship-button');

        $buttons.click(function () {
            $header.addClass('opened');
            $buttons.addClass('transparent');
            $(this).removeClass('transparent');
            $tourPaginationList.removeClass('active');
            $tourPaginationList.filter($(this).attr('data-ship')).addClass('active');

            return false;
        });
        if (!$header.hasClass('opened')) {
            $buttons.filter('.active').click();
        }
    }


    function initTourPagination() {
        var $pagination = $('.tour-top-pagination'),
            $shipImageWrapper = $('.avaliable-shipimage'),
            shipImageActiveClass = 'active-shipimage-item',
            itemActiveClass = 'tour-pagination-item--active',
            $buttons = $('.button-triangle'),
            buttonActiveClass = 'active',
            currentSlide = 0,
            slidesCount = $pagination.length;

        var currentNum = $('.tour-pagination-item--active');
        $('.tour-pagination-item').each(function (i) {
            if ($(this).hasClass('tour-pagination-item--active')) {
                currentSlide = i;
            }
        });

        $pagination.slick({
            slidesToShow: 6,
            centerMode: true,
            centerPadding: 0,
            focusOnSelect: true,
            initialSlide: currentSlide,
            prevArrow: '<span class="tour-pagination-item--prev"><a class="tour-pagination-link" href="#"></a></span>',
            nextArrow: '<span class="tour-pagination-item--next"><a class="tour-pagination-link" href="#"></a></span>',
            responsive: [{
                breakpoint: 1200,
                settings: {
                    slidesToShow: 5,
                }
            }, {
                breakpoint: 768,
                settings: {
                    slidesToShow: 3,
                }
            }, {
                breakpoint: 480,
                settings: {
                    //centerMode: true,
                    slidesToShow: 3,
                    //centerPadding: 0
                }
            }]
        });

        // if ($pagination.hasClass('js-main-tour-selector')) {
        //     $pagination.find('.js-tour-selector-item').click(function() {
        //         setCurrentElement($(this).parent());
        //         $shipImageWrapper.removeClass('two-ships');
        //         setGoButton(this);
        //         setShipImage(this);
        //         var sliderNumber = $(this).parent().attr('data-slick-index');
        //         sliderNumber = sliderNumber < 0 ? slidesCount + sliderNumber : sliderNumber;
        //         $pagination.slick('slickGoTo', $(this).parent().attr('data-slick-index'));
        //         return false;
        //     });
        //     $pagination.on('afterChange', function(event, slick, currentSlide) {
        //         var $slide = $pagination.find('.slick-center');
        //         setCurrentElement($slide);
        //         setGoButton($slide.find('.js-tour-selector-item'));
        //         setShipImage($slide.find('.js-tour-selector-item'));
        //     });

        // } else {
        //     $pagination.find('.js-tour-selector-item').click(function() {
        //         var targetURL = $(this).attr('href');
        //         var curentURL = window.location.href;
        //         var hash = curentURL.substring(curentURL.indexOf("#"), curentURL.length);
        //         targetURL = hash === 0 ? targetURL : targetURL + hash;
        //         window.location.href = targetURL;
        //         return false;
        //     })

        // }

        function setCurrentElement(item) {
            $pagination.find('.' + itemActiveClass).removeClass(itemActiveClass);
            $(item).addClass(itemActiveClass);
        }

        function setGoButton(item) {
            var selectedButtonClass = $(item).attr('data-button');
            $buttons.filter('.' + buttonActiveClass).removeClass(buttonActiveClass);
            $buttons.filter('.' + selectedButtonClass).addClass(buttonActiveClass);
        }

        function setShipImage(item) {
            var selectedItemClass = $(item).attr('data-ship');
            $shipImageWrapper.find('.' + shipImageActiveClass).removeClass(shipImageActiveClass);
            $shipImageWrapper.find('.' + selectedItemClass).addClass(shipImageActiveClass);
        }
    }

    function initToursTab() {
        var tabItem = $('.tours-left').find('li');
        var tabContent = $('#tours-section').find('.tours-content');
        var mainBg = $('.tours-right');
        var coordinateS = mainBg.find('.coordinate-s');
        var coordinateE = mainBg.find('.coordinate-e');

        tabItem.each(function (i) {
            $(this).attr('data-count', i);
        });

        tabContent.each(function (i) {
            $(this).attr('data-count-content', i);
        });

        tabItem.click(function (e) {
            e.preventDefault();
            var currentTabTitle = $(this).attr('data-count');
            var currentTabContent = tabContent.eq(currentTabTitle);
            var currentTabContentBg = currentTabContent.css('background-image');
            var currentTabCoordinateS = currentTabContent.attr('data-coordinate-s');
            var currentTabCoordinateE = currentTabContent.attr('data-coordinate-e');

            tabItem.removeClass('active-menu');
            $(this).addClass('active-menu');
            mainBg.css('background-image', currentTabContentBg);
            coordinateS.addClass('anim');           
            coordinateE.addClass('anim');           

            setTimeout(function() {
                coordinateS.removeClass('anim');
                coordinateE.removeClass('anim');
            }, 700);


            coordinateS.text(currentTabCoordinateS);
            coordinateE.text(currentTabCoordinateE);
            tabContent.hide();
            currentTabContent.show();
        });

        tabItem.eq(0).click();
    }

    function initShipTourTab(selector, activeTabClass) {
        var tabSelector = '.js-tabs-item';
        var activeTabClass = activeTabClass || 'tab-active';
        var $items = $(selector);

        $items.each(function () {
            var $tabContentList;
            var $tabs = $(this).find(tabSelector)
            $tabs.each(function () {

                var $tabContentItem = $($(this).attr('data-tab-selector'));

                if ($tabContentList === undefined) {
                    $tabContentList = $tabContentItem;
                } else {
                    $tabContentList = $tabContentList.add($tabContentItem);
                }

                $tabContentItem.hide();
            });

            $tabs.click(function () {
                $tabs.removeClass(activeTabClass);
                $(this).addClass(activeTabClass);
                $tabContentList.hide();
                $($(this).attr('data-tab-selector')).show();
                $($(this).attr('data-tab-selector')).find('.main-nav-list-item').first().trigger('click');
            });

            $tabs.eq(0).click();
        });

        //
    }

    function initShipSlider() {
        var $slider = $('#ship-slider');

        if ($slider.length == 0) {
            return;
        }

        $slider.slick({
            prevArrow: '<button type="button" class="slick-prev"></button>',
            nextArrow: '<button type="button" class="slick-next"></button>',
            slidesToShow: 6,
            responsive: [{
                breakpoint: 992,
                settings: {
                    slidesToShow: 4
                }
            }, {
                breakpoint: 601,
                settings: {
                    slidesToShow: 1
                }
            }]
        });

        $slider.magnificPopup({
            delegate: '.bg', // child items selector, by clicking on it popup will open
            type: 'image',
            gallery: {
                enabled: true
            }
        });
    }

    function toggleMenu() {
        var $menuToggle = $('#menu-toggle');
        var $mainMenu = $('#main-menu');
        var $overlay = $('#overlay');
        var $closeMainMenu = $('#close-main-menu');

        closeMenu = closeMainMenu;


        $menuToggle.click(openMainMenu);
        $closeMainMenu.click(closeMainMenu);
        $overlay.click(closeMainMenu);

        function openMainMenu() {
            $overlay.fadeIn(500);
            $closeMainMenu.fadeIn(2000);
            $mainMenu.show();
        }

        function closeMainMenu() {
            $mainMenu.hide();
            $closeMainMenu.fadeOut();
            $overlay.fadeOut(500);
        }
    }

    function animateCoordinate() {
        var $coordinateNumber = $('.anim-coordinate');
        $coordinateNumber.each(function () {
            $(this).prop('Counter', 0).animate({
                Counter: $(this).text()
            }, {
                duration: 3000,
                easing: 'swing',
                step: function (now) {
                    var num = Math.ceil(now).toString();
                    if (num.split('').length < 2) {
                        num = '0' + num;
                    }
                    $(this).text(num);
                }
            });
        });
    }

    function initPageScroll() {
        var isInitialized = false,
            MOBILESIZE = 991,
            $body = $('body'),
            $allSections = $('.section'),
            sectionActiveClass = 'animated',
            $pagiMenuElements = $('#pagiMenu').find('li'),
            $aboutCapLink = $('.about-cap-link'),
            $mainTourAncor = $('#mainNav').find('li'),
            $bookButton = $('#book-button'),
            $footer = $('.footer-main');



        if ($body.attr('data-full-page') !== 'true') {
            return false;
        }

        setFullPageState();

        $(window).on('resize', setFullPageState);

        $('.scroll-bottom').click(function (e) {
            $(window).trigger('mousewheel', -1);
            return false;
        });

        function setFullPageState() {
            if (window.innerWidth > MOBILESIZE && !isInitialized) {
                $body.addClass('single-screen');
                $body.addClass($allSections.filter('.animated').data('anchor'));
                isInitialized = true;
            } else if (window.innerWidth <= MOBILESIZE && isInitialized) {
                $body.removeClass('single-screen');
                isInitialized = false;
            }
        }

        var isAnimationOver = true;
        $(window).mousewheel(function (e, direction) {
            if (isInitialized && isAnimationOver) {
                e.stopPropagation();
                e.preventDefault();
                isAnimationOver = false;
                var $activeSection = $allSections.filter('.' + sectionActiveClass),
                    $target;
                direction = direction > 0 ? 'up' : 'down';
                switch (direction) {
                    case 'up':
                        $target = $activeSection.prev('.section');
                        break;
                    case 'down':
                        $target = $activeSection.next('.section');
                        break;
                }
                if ($target.length != 0) {
                    if ($activeSection.hasClass('up')) {
                        $activeSection.removeClass('up');
                        $footer.removeClass('up');
                        $body.removeClass('visible-footer');
                        setTimeout(function () {
                            isAnimationOver = true;
                        }, 2000);
                        return false;
                    }

                    if($body.hasClass('trips') && !isFirstSection($target.data('anchor'))){
                        $('.tour-locations-header').fadeOut();
                    }else{
                        $('.tour-locations-header').fadeIn();
                    }

                    if (isLastSection($target.data('anchor'))) {
                        $body.addClass('last-step');
                    } else {
                        $body.removeClass('last-step');
                    }

                    $body.removeClass($activeSection.data('anchor')).addClass($target.data('anchor'));

                    $activeSection.removeClass(sectionActiveClass);
                    $target.addClass(sectionActiveClass);

                    $pagiMenuElements.removeClass('active');
                    $pagiMenuElements.filter('[data-menuanchor="' + $target.data('anchor') + '"]').addClass('active');

                    var videoId;

                    if ($activeSection.hasClass('video-section')) {
                        videoId = $activeSection.find('.video-wrapper').find('div').attr('id');
                        jwplayer(videoId).pause();
                    }

                    if ($target.hasClass('video-section')) {
                        videoId = $target.find('.video-wrapper').find('div').attr('id');
                        jwplayer(videoId).play();
                    }
                } else if ($body.hasClass('last-step') && $body.hasClass('footer-scroll')) {
                    $allSections.last().addClass('up');
                    $body.addClass('visible-footer');
                    $footer.addClass('up');
                }
                setTimeout(function () {
                    isAnimationOver = true;
                }, 2000);
                return false;
            }
        });

        $pagiMenuElements.click(function (e) {
            e.preventDefault();
            var anchor = $(this).data('menuanchor'),
                $activeSection = $allSections.filter('.' + sectionActiveClass),
                $target = $allSections.filter('[data-anchor="' + anchor + '"]');
            $body.removeClass($activeSection.data('anchor')).addClass(anchor);

            $activeSection.removeClass(sectionActiveClass);
            $target.addClass(sectionActiveClass);

            $pagiMenuElements.removeClass('active');
            $(this).addClass('active');

            if($body.hasClass('trips') && !isFirstSection($target.data('anchor'))){
                $('.tour-locations-header').fadeOut();
            }else{
                $('.tour-locations-header').fadeIn();
            }

            if (isLastSection(anchor)) {
                $body.addClass('last-step');
            } else {
                $body.removeClass('last-step')
            }
        });

        $bookButton.click(function (e) {
            var anchor = $(this).data('menuanchor'),
                $activeSection = $allSections.filter('.' + sectionActiveClass),
                $target = $allSections.filter('[data-anchor="' + anchor + '"]');
            $body.removeClass($activeSection.data('anchor')).addClass(anchor);

            $activeSection.removeClass(sectionActiveClass);
            $target.addClass(sectionActiveClass);

            $pagiMenuElements.removeClass('active');
            $pagiMenuElements.filter(function (index) {
                if ($(this).data('menuanchor') == anchor) {
                    return $(this).addClass('active');
                }
            });

            closeMenu();


            if (isLastSection(anchor)) {
                $body.addClass('last-step');
            } else {
                $body.removeClass('last-step')
            }
        });

        $aboutCapLink.click(function (e) {
            e.preventDefault();
            var anchor = $(this).data('menuanchor'),
                $activeSection = $allSections.filter('.' + sectionActiveClass),
                $target = $allSections.filter('[data-anchor="' + anchor + '"]');
            $body.removeClass($activeSection.data('anchor')).addClass(anchor);

            $activeSection.removeClass(sectionActiveClass);
            $target.addClass(sectionActiveClass);

            $pagiMenuElements.removeClass('active');
            $pagiMenuElements.filter(function (index) {
                if ($(this).data('menuanchor') == anchor) {
                    return $(this).addClass('active');
                }
            });


            if (isLastSection(anchor)) {
                $body.addClass('last-step');
            } else {
                $body.removeClass('last-step')
            }
        });         



        if (window.innerWidth > MOBILESIZE) {
            tourLinkScroll();            
            // $(window).on('resize', tourLinkScroll);
        }


        if (window.innerWidth < MOBILESIZE) {
            mobileScrollToTour();        
        }

        function tourLinkScroll() {
            $mainTourAncor.click(function (e) {
                var anchor = $(this).data('menuanchor'),
                    $activeSection = $allSections.filter('.' + sectionActiveClass),
                    $target = $allSections.filter('[data-anchor="' + anchor + '"]');
                $body.removeClass($activeSection.data('anchor')).addClass(anchor);

                $activeSection.removeClass(sectionActiveClass);
                $target.addClass(sectionActiveClass);

                $pagiMenuElements.removeClass('active');
                $pagiMenuElements.filter(function (index) {
                    if ($(this).data('menuanchor') == anchor) {
                        return $(this).addClass('active');
                    }
                });

                closeMenu();


                if (isLastSection(anchor)) {
                    $body.addClass('last-step');
                } else {
                    $body.removeClass('last-step')
                }
            });    
        }

        function mobileScrollToTour() {
            var $toursSection = $('#tours-section');
            var $toursLinkSection = $('#tours-link-section');
            var toursSectionPosition = $toursSection.offset().top;

            $toursLinkSection.click(function(){
                $('html, body').animate({
                    scrollTop: toursSectionPosition
                }, 700);
                closeMenu();
            });

            $(window).on('resize', function() {
                toursSectionPosition = $toursSection.offset().top;
            });
        }

        function isLastSection(anchor) {
            return anchor == $allSections.last().data('anchor');
        }

        function isFirstSection(anchor) {
            return anchor == $allSections.first().data('anchor');
        }

    }

    function initOverlay() {
        var $menuToggle = $('#menu-toggle');
        var $overlay = $('#overlay');
        $menuToggle.click(function () {
            $overlay.fadeToggle(500);
        });
    }

    function mobileTourMenu() {
        var $tourMenu = $('.mobile-tour-menu'),
            $tourTitle = $('.current-menu'),
            $tourOptions = $('.mobile-tour-options'),
            $tourMenuItem = $('.mobile-tour-item');

        if ($tourMenu.length > 0) {

            $.each($tourTitle, function (i, title) {
                var sectionId = $(title).parents('.section').attr('data-anchor'),
                    titleText = $tourOptions.find('a[href="#' + sectionId + '"]').html();
                $(title).html(titleText);
            });

            $tourTitle.click(function () {
                $tourOptions.stop().slideUp();
                $(this).next('.mobile-tour-options').stop().slideToggle();
            });

            $tourMenuItem.click(function (e) {
                e.preventDefault();
                var link = $(this).attr('href');
                link = link.substring(1, link.length);
                $tourOptions.stop().slideUp();
                $('html, body').animate({
                    scrollTop: $('.section[data-anchor="' + link + '"]').offset().top - 130
                }, 500);
            });

            $(document).click(function (event) {
                if ($(event.target).closest($tourTitle).get(0) == null) {
                    $tourOptions.stop().slideUp();
                }
            });
        }

    }

    function initFishSlider() {
        var $mainSlider = $('#fish');
        var $navSlider = $('#fish-nav');
        var $accommodationSlider = $('#accommodation-slider');
        var $typeFishNav = $('#type-fish-nav');
        var $typeFishFor = $('#fishing-for-slide');
        var $typeTripNav = $('#type-trip-nav');
        var $typeTripFor = $('#trip-for-slide');

        $mainSlider.slick({
            prevArrow: '<button type="button" class="slick-prev"></button>',
            nextArrow: '<button type="button" class="slick-next"></button>',
            slidesToShow: 3,
            autoplaySpeed: 4000,
            autoplay: true,
            slidesToScroll: 1,
            centerMode: true,
            centerPadding: '0px',
            asNavFor: $navSlider,
            responsive: [{
                breakpoint: 1200,
                settings: {
                    slidesToShow: 2
                }
            }, {
                breakpoint: 601,
                settings: {
                    slidesToShow: 1
                }
            }]
        });

        $navSlider.slick({
            arrows: false,
            slidesToShow: 7,
            slidesToScroll: 1,
            autoplay: true,
            dots: false,
            asNavFor: $mainSlider,
            focusOnSelect: true,
            autoplaySpeed: 6000,
            responsive: [{
                breakpoint: 1200,
                settings: {
                    slidesToShow: 3
                }
            }, {
                breakpoint: 601,
                settings: {
                    slidesToShow: 1
                }
            }]
        });

        $accommodationSlider.slick({
            prevArrow: '<button type="button" class="slick-prev"></button>',
            nextArrow: '<button type="button" class="slick-next"></button>',
            slidesToShow: 1,
            slidesToScroll: 1
        });

        $typeFishNav.slick({
            arrows: true,
            slidesToShow: 7,
            slidesToScroll: 1,
            asNavFor: $typeFishFor,
            focusOnSelect: true,
            prevArrow: '<button type="button" class="slick-prev"></button>',
            nextArrow: '<button type="button" class="slick-next"></button>',
            responsive: [{
                breakpoint: 1400,
                settings: {
                    slidesToShow: 5,
                    centerMode: true,
                    centerPadding: '0px'
                }
            }, {
                breakpoint: 992,
                settings: {
                    slidesToShow: 3,
                    centerMode: true,
                    centerPadding: '0px'
                }
            }, {
                breakpoint: 601,
                settings: {
                    slidesToShow: 1,
                    centerMode: true,
                    centerPadding: '0px'
                }
            }]
        });

        $typeFishFor.slick({
            draggable: false,
            arrows: false,
            slidesToShow: 1,
            slidesToScroll: 1,
            fade: true,
            cssEase: 'linear',
            asNavFor: $typeFishNav
        });

        $typeTripNav.slick({
            arrows: true,
            slidesToShow: 4,
            slidesToScroll: 1,
            asNavFor: $typeTripFor,
            focusOnSelect: true,
            prevArrow: '<button type="button" class="slick-prev"></button>',
            nextArrow: '<button type="button" class="slick-next"></button>',
            responsive: [{
                breakpoint: 992,
                settings: {
                    slidesToShow: 3,
                    centerMode: true,
                    centerPadding: '0px'
                }
            }, {
                breakpoint: 601,
                settings: {
                    slidesToShow: 1,
                    centerMode: true,
                    centerPadding: '0px'
                }
            }]
        });

        $typeTripFor.slick({
            draggable: false,
            arrows: false,
            slidesToShow: 1,
            slidesToScroll: 1,
            fade: true,
            cssEase: 'linear',
            asNavFor: $typeTripNav
        });
    }

    function initDatePicker() {

        var $datePicker = $('#date-picker');
        if ($datePicker.length == 0) {
            return;
        }

        $.fn.datepicker.language['en'] = {
            days: ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'],
            daysShort: ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'],
            daysMin: ['Su', 'Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa'],
            months: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
            monthsShort: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
            today: 'Today',
            clear: 'Clear',
            dateFormat: 'mm/dd/yy',
            timeFormat: 'hh:ii aa',
            firstDay: 0
        };

        $('#date-picker').datepicker({
            language: 'en',
            range: true,
            offset: -180,
            position: 'left center'
        });
    }

    function initBookForm() {
        var $form = $('#booking-form').find('form'),
            droppedFiles = false,
            $filesInput = $form.find('input[type="file"]'),
            stepClasses = ['step1-active', 'step2-active', 'step3-active', 'step4-active', 'step5-active'],
            $formStepsWidget = $('.form-steps-list'),
            currentStep = 0,
            $formBody = $('#booking-form'),
            $steps = $('.step');

        if ($form.length < 1) {
            return;
        }

        $('input[type="text"], input[type="tel"], input[type="email"]').keypress(function (e) {
            if (e.which == 13) {
                return false;
            }
        });


        var $text = $form.find('.dropzone').find('.text'),
            a = $text.children('a');

        $text.before('<div class="text"><div>DRAG & DROP PHOTOS HERE</div><div>or</div><a href="#" id="manual-select-files">Select files</a></div>');

        $('#manual-select-files').click(function () {
            a.click();
            return false;
        });

        setStep(0);

        $filesInput.click(function (e) {
            e.stopPropagation();
        });

        $form.find('.next-btn').click(function () {
            $('.error').removeClass('error');
            switch (currentStep) {
                case 0:
                    if (validateFirstStep()) {
                        setStep(currentStep + 1);
                    }
                    return false;
                case 1:
                    if (validateSecondStep()) {
                        setStep(currentStep + 1);
                    }
                    return false;
                case 2:
                    setStep(currentStep + 1);
                    return false;
                case 3:
                    var $form = $formBody.find('form');
                    $.ajax({
                        url: $form.attr('action'),
                        data: $form.serialize(),
                        dataType: 'json',
                        type: 'post',
                        success: function (response) {
                            if (response.status == 'success') {
                                setStep(currentStep + 1);
                            }
                        }
                    });
                    break;
            }

            return false;
        });

        $form.find('.back-btn').click(function () {
            setStep(currentStep - 1);
            return false;
        });

        function setStep(number) {
            currentStep = number;
            $formBody.removeClass(stepClasses.join(' ')).addClass(stepClasses[currentStep]);
            $formStepsWidget.removeClass(stepClasses.join(' ')).addClass(stepClasses[currentStep]);
        }

        function validateFirstStep() {
            var stepContainer = $('.step1'),
                $nameInputs = stepContainer.find('input[type="text"]'),
                $emailInput = stepContainer.find('input[type="email"]'),
                $phoneInput = stepContainer.find('input[type="tel"]'),
                nameRegExp = /^[a-z]+$/i,
                phoneRegExp = /^\({0,1}((0|\+61)(2|4|3|7|8)){0,1}\){0,1}(\ |-){0,1}[0-9]{2}(\ |-){0,1}[0-9]{2}(\ |-){0,1}[0-9]{1}(\ |-){0,1}[0-9]{3}$/,
                emailRexExp = /^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/,
                isValid = true;
            $.each($nameInputs, function (i, input) {
                if (!nameRegExp.test($(input).val())) {
                    $(input).addClass('error');
                    isValid = false;
                }
            });
            if (!phoneRegExp.test($phoneInput.val())) {
                $phoneInput.addClass('error');
                isValid = false;
            }
            if (!emailRexExp.test($emailInput.val())) {
                $emailInput.addClass('error');
                isValid = false;
            }
            return isValid;
        }

        function validateSecondStep() {
            $('.error').removeClass('error');
            var $peopleNumber = $('#people-number'),
                $date = $('#date-picker'),
                isValid = true;
            if ($peopleNumber.val() === '' || +$peopleNumber.val() < 1 || +$peopleNumber.val() % 1 !== 0) {
                $peopleNumber.parents('.form-item-label').addClass('error');
                isValid = false;
            }
            if ($date.val() === '') {
                $date.parents('.form-item-label').addClass('error');
                isValid = false;
            }
            return isValid;

        }
    }

    function initContactForm() {
        var $contactForm = $('#contact-form');
        $contactForm.on('submit', function (event) {
            $(this).find('.error').removeClass('error');
            $(this).find('.success').removeClass('success').text('SEND');
            event.preventDefault();
            var $nameFrom = $(this).find('input[name="name"]'),
                $message = $(this).find('#message'),
                nameRegexp = /^[a-z]+$/i;
            if (!nameRegexp.test($nameFrom.val())) {
                $nameFrom.addClass('error');
                return;
            }
            if ($message.val() == '') {
                $message.addClass('error');
                return;
            }
            var $form = $(this);
            $.ajax({
                url: $form.attr('action'),
                dataType: 'json',
                data: $form.serialize(),
                type: 'post',
                success: function (response) {
                    if (response.status == 'success') {
                        $form.find('.button').addClass('success').text('DONE');
                    }
                }
            });
        })
    }

})(jQuery);